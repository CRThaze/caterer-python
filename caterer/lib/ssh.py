# code: utf-8
#

import paramiko

def get_connection(hostname, username, password, port):
    """Open a connection to the remote sever and return the client object"""

    # setup ssh client object
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # opening connection
    print "[caterer] opening connection..."

    try:
        if username == None and password == None:
            client.connect(hostname, port)

        elif username != None and password == None:
            client.connect(hostname, port, username)

        elif username == None and password != None:
            client.connect(hostname, port, password=password)

        elif username != None and password != None:
            client.connect(hostname, port, username, password)
    except Exception:
        print "[caterer] ERROR! connection attempt failed."
        quit(1)

    return client
