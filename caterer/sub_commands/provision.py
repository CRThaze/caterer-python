# code: utf-8
#

import paramiko
import pexpect
from ..lib import ssh

def provision(client, hostname, username, password, repo_path, cookbooks_path, roles_path, data_bags_path, runlist, port):
    """Run the Provision Sub Command for Caterer"""
    
    # make sure that /tmp/caterer & /tmp/caterer/cache exist on remote server
    print "[caterer] setting up cache..."

    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("sudo mkdir -p /tmp/caterer/cache")
    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: failed to create /tmp/caterer/cache"
        quit(1)

    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("sudo chmod -R 777 /tmp/caterer")
    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: failed to set permissions on /tmp/caterer"
        quit(1)

    if repo_path != None:

        if repo_path[-1:] != "/":
            repo_path += "/"

        cookbooks_path = repo_path + "/cookbooks"
        roles_path = repo_path + "/roles"
        data_bags_path = data_bags_path + "/data_bags"
    
    else:
        # Sanitize directory paths
        if cookbooks_path[-1:] == "/":
            cookbooks_path = cookbooks_path[:-1]
        if roles_path[-1:] == "/":
            roles_path = roles_path[:-1]
        if data_bags_path[-1:] == "/":
            data_bags_path = data_bags_path[:-1]


    # Sync folders on remote server

    # sync cookbooks
    print "[caterer] syncing cookbooks..."

    if username != None:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s@%s:/tmp/caterer/" % (port, cookbooks_path, username, hostname)) )
    else:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s:/tmp/caterer/" % (port, cookbooks_path, hostname)) )
    if password != None:
        child.expect ('password:')
        child.sendline (password)

    while child.isalive():
        pass
    
    # sync roles
    print "[caterer] syncing roles..."
    
    if username != None:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s@%s:/tmp/caterer/" % (port, roles_path, username, hostname)) )
    else:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s:/tmp/caterer/" % (port, roles_path, hostname)) )
    if password != None:
        child.expect ('password:')
        child.sendline (password)

    while child.isalive():
        pass

    # sync data_bags
    print "[caterer] syncing data_bags..."
    
    if username != None:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s@%s:/tmp/caterer/" % (port, data_bags_path, username, hostname)) )
    else:
        child = pexpect.spawn( ("rsync -zr -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p %s' --delete %s %s:/tmp/caterer/" % (port, data_bags_path, hostname)) )
    if password != None:
        child.expect ('password:')
        child.sendline (password)

    while child.isalive():
        pass


    # Trigger Chef-Solo Run!
    print "[caterer] triggering chef run..."

    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command( "sudo chef-solo -c /etc/chef/caterer/solo.rb -o \"" + "".join(runlist) + "\"" )

    # stream output of chef run
    sentinel = True
    while sentinel:
        data = chan.recv(1024)
        print data,
        if data == "":
            sentinel = False

    if chan.recv_exit_status() != 0:
        quit(1)


def execute(hostname, username, password, port, repo_path, cookbooks_path, roles_path, data_bags_path, runlist):
    """Connect & Run the Provision Sub Command for Caterer"""

    client = ssh.get_connection(hostname, username, password, port)

    provision(client, hostname, username, password, repo_path, cookbooks_path, roles_path, data_bags_path, runlist, port)


