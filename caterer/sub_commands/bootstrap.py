# code: utf-8
#

import paramiko
from ..lib import ssh

def bootstrap(client, scripts_path, script):
    """Run the Bootstrap Sub Command for Caterer"""

    # install chef
    print "[caterer] installing chef..."

    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("if [ ! `which chef-solo` ]; then sudo curl -L http://www.opscode.com/chef/install.sh | sudo bash; fi")
    
    # stream output of chef install
    sentinel = True
    while sentinel:
        data = chan.recv(1024)
        print data,
        if data == "":
            sentinel = False

    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: failed to install chef on host!"
        quit(1)


    # create caterer directory in /etc/chef
    print "[caterer] configuring caterer environment..."

    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("sudo mkdir -p /etc/chef/caterer")
    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: failed to create /etc/chef/caterer on host!"
        quit(1)

    # configure chef-solo
    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("""sudo bash -c "echo -e 'file_cache_path \\"/tmp/caterer/cache\\"\\ncookbook_path \\"/tmp/caterer/cookbooks\\"\\nrole_path \\"/tmp/caterer/roles\\"\\ndata_bag_path \\"/tmp/caterer/data_bags\\"\\n' > /etc/chef/caterer/solo.rb" """)
    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: failed to create configuration file!"
        quit(1)

    # run bootstrap hook script
    if script != None:
        print "[caterer] runing bootstrap script..."

        if script[-3:] != ".sh":
            script += ".sh"

        if scripts_path[-1:] != "/":
            scripts_path += "/"

        chan = client.get_transport().open_session()
        chan.get_pty()
        chan.exec_command("sudo bash -c '" + open(scripts_path + script, 'r').read() + "'")

        # stream output of bootstrap script
        sentinel = True
        while sentinel:
            data = chan.recv(1024)
            print data,
            if data == "":
                sentinel = False

        if chan.recv_exit_status() != 0:
            print "[caterer] ERROR: bootstrap script failed!"
            quit(1)


def execute(hostname, username, password, port, scripts_path, script):
    """Connect & Run the Bootstrap Sub Command for Caterer"""

    client = ssh.get_connection(hostname, username, password, port)

    bootstrap(client, scripts_path, script)


