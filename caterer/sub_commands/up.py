# code: utf-8
#

import paramiko
from . import bootstrap as boot
from . import provision as prov
from ..lib import ssh

def execute(hostname, username, password, port, scripts_path, script, repo_path, cookbooks_path, roles_path, data_bags_path, runlist):
    """Connect & Execute bootstrap & provision sub commands"""

    client = ssh.get_connection(hostname, username, password, port)

    boot.bootstrap(client, scripts_path, script)
    prov.provision(client, hostname, username, password, repo_path, cookbooks_path, roles_path, data_bags_path, runlist, port)

