# code: utf-8
#

import paramiko
from ..lib import ssh

def reboot(client):
    """Reboot the remote server"""
    
    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("sudo shutdown -r now")
    
    # stream output of chef install
    sentinel = True
    while sentinel:
        data = chan.recv(1024)
        print data,
        if data == "":
            sentinel = False
    
    if chan.recv_exit_status() != 0:
        print "[caterer] ERROR: shutdown command exited with non-zero exit code!  (weird...)"
        quit(1)


def execute(hostname, username, password, port):
    """Connect & Run the Reboot Sub Command for Caterer"""

    client = ssh.get_connection(hostname, username, password, port)

    reboot(client)
