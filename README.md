Caterer
=====
*The Chef who comes to you*

About
--------
Chef is a push based tool designed to provide a lightweight replacement for Chef-Server & Knife.  It leverages Chef-Solo & rsync to provision remote hosts from one or many management workstations.  Instead of relying on a Chef-Server it uses a local repository of cookbooks, roles, etc., on your workstation, to provision remote hosts through Chef-Solo.  In this way it's similar to the Knife-Solo plugin but does not require Chef to be installed on the local machine.  

Caterer is designed to use a workflow similar to Vagrant, which allows Vagrant users to easily transition from testing to production and find the same ease of use.

Installation
---------------
####distutils:

To install from the project directory:

`sudo python setup.py install`

####pip:

or, using `pip` (which will provide an easy way to uninstall):

`python setup.py sdist`

`sudo pip install dist/caterer-x.x.x.tar.gz`

and then you can uninstall with:

`sudo pip uninstall caterer`


Usage
---------
Caterer provides several sub-commands which are used to interact with the remote host you wish to 'chef': 

* `cater bootstrap HOST [options]`
* `cater provision HOST [options]`
* `cater up HOST [options]`
* `cater reboot HOST [options]`

  All these commands will accept any of the following options:
    * `-a USERNAME, --as USERNAME` if omitted will assume current username
    * `-x PASSWORD, --password PASSWORD` if omitted will assume key authentication
    * `-p PORTNUM, --port PORTNUM` if omitted will default to port 22

####`bootstrap`
Used to setup the caterer environment on the remote host.  It will also allow you to provide a specific script to run while bootstrapping (in case you want to do anything additional when preparing the node to run chef): `-b my_bootstrap_script`  It will assume that it can find this script inside a folder named `bootstrap/` in the current directory.  Otherwise you can specify a specific directory with the `-B` option, like so: `-B /home/user/caterer/bootstraps -b my_bootstrap_script`

####`provision`
Used to run chef on the remote host. Be sure to also provide a run_list or your chef-run will not do anything: `-l "recipe[test], role[ubuntu_default]"`.  Caterer will expect to find your cookbooks, roles & data _bags in their own directories within the current working directory.  Otherwise you will have to specify where to find them with the `-c, --cookbooks`, `-r, --roles` & `-d, --data_bags` options.  Alternately, if you have all of these directories inside a caterer/chef repo, you can simply specify the path to that directory: `-R /home/user/caterer`

####`up`
A shortcut command to both `bootstrap` & `provision` a remote host.  Will accept all the options that either of those two commands have.

####`reboot`
A convenience command to reboot a remote server.

###Example:

To set up the caterer environment:

    my_laptop$ cater bootstrap 192.168.13.157 -a root -x rootpass -b my_bootstrap

To run chef on the remote host:

    my_laptop$ cater provision 192.168.13.157 -a root -x rootpass -l "recipe[test], role[ubuntu_default]"

To reboot the server:

    my_laptop$ cater reboot 192.168.13.157 -a root -x rootpass

Author:
----------
Diego Carrión (0x783czar)

License:
-----------
Copyright (C) 2012 PagodaBox Inc.

Except as contained in this notice, the name(s) of the above copyright holders shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
