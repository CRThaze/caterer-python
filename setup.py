from distutils.core import setup
setup(
    name='caterer',
    version='0.0.5',
    packages=[
        'caterer',
        'caterer.sub_commands',
        'caterer.lib'
    ],
    scripts=[
        'bin/cater'
    ],
    author='0x783czar (Diego Carrion)',
    author_email='diego@pagodabox.com',
    license='LICENSE.txt',
    description='A push based tool for managing chef configured nodes.',
    requires=[
        "paramiko",
        "pexpect"
    ],
)
